//
//  Repository.swift
//  JavaPop
//
//  Created by Mariana Alvarez on 28/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import Foundation

public class Repository {
    
    var repoName: String!
    var repoDescription: String!
    var username: String!
    var userPhoto: String!
    var numberOfForks: Int!
    var numberOfStars: Int!

}
