//
//  RepoTableViewController.swift
//  JavaPop
//
//  Created by Mariana Alvarez on 27/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class RepoTableViewController: UITableViewController {
    
    var repoManager = RepoManager.sharedInstance
    let imageCache = AutoPurgingImageCache()
    let request: Request! = nil
    
    var activityIndicador: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()

        self.activityIndicador = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        self.activityIndicador.color = UIColor.black
        self.activityIndicador.center.x = self.view.center.x
        self.activityIndicador.center.y = self.view.center.y * 0.5
        self.activityIndicador.hidesWhenStopped = true
        self.view.addSubview(activityIndicador)
        
        // Resizes UITableView to fit its content
        self.tableView.estimatedRowHeight = 135
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        
        // Get all repositories from page 1
        self.getRepositories()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repoManager.repositoryList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repoCell", for: indexPath) as! RepoTableViewCell

        cell.username.text = repoManager.repositoryList[indexPath.row].username
        cell.repoName.text = repoManager.repositoryList[indexPath.row].repoName
        cell.repoDescription.text = repoManager.repositoryList[indexPath.row].repoDescription
        cell.numberOfForks.text = "\(repoManager.repositoryList[indexPath.row].numberOfForks!)"
        cell.numberOfStars.text = "\(repoManager.repositoryList[indexPath.row].numberOfStars!)"
        
        let imageURL = repoManager.repositoryList[indexPath.row].userPhoto
        
        cell.userPhoto.clipsToBounds = true
        cell.userPhoto.layer.borderWidth = 1
        cell.userPhoto.layer.borderColor = UIColor.white.cgColor
        cell.userPhoto.layer.cornerRadius = 10
        cell.setImage(url: imageURL!)

        return cell

    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == repoManager.repositoryList.count - 1 {
            repoManager.currentPage += 1
            self.getRepositories()
        }
    }
    
    // MARK: - Access to Manager
    
    // Fetching repositories
    func getRepositories() {
        self.activityIndicador.startAnimating()
        self.repoManager.getRepositories(callback: { (repositories) -> Void in
            self.activityIndicador.stopAnimating()
            self.tableView.reloadData()
        })
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            let cell = sender as! UITableViewCell
            let indexPath = self.tableView.indexPath(for: cell)
            let vc = segue.destination as! DetailTableViewController
            
            vc.name = self.repoManager.repositoryList[indexPath!.row].username
            vc.repository = self.repoManager.repositoryList[indexPath!.row].repoName
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            self.navigationItem.backBarButtonItem =  backItem
        }
        
    }
}
