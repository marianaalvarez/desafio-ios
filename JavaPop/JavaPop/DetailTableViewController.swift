//
//  DetailTableViewController.swift
//  JavaPop
//
//  Created by Mariana Alvarez on 27/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {

    var name: String!
    var repository: String!
    var repoManager = RepoManager.sharedInstance
    var activityIndicador: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = repository
        self.tableView.tableFooterView = UIView()
        
        self.activityIndicador = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        self.activityIndicador.color = UIColor.black
        self.activityIndicador.center.x = self.view.center.x
        self.activityIndicador.center.y = self.view.center.y * 0.5
        self.activityIndicador.hidesWhenStopped = true
        self.view.addSubview(activityIndicador)
        
        // Resizes UITableView to fit its content 
        self.tableView.estimatedRowHeight = 135
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        
        self.repoManager.pullRequestList.removeAll()
        self.getPullRequests()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repoManager.pullRequestList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailTableViewCell

        cell.title.text = repoManager.pullRequestList[indexPath.row].title
        cell.body.text = repoManager.pullRequestList[indexPath.row].body
        cell.username.text = repoManager.pullRequestList[indexPath.row].username
        
        let imageURL = repoManager.pullRequestList[indexPath.row].userPhoto
        
        cell.userPhoto.clipsToBounds = true
        cell.userPhoto.layer.borderWidth = 1
        cell.userPhoto.layer.borderColor = UIColor.white.cgColor
        cell.userPhoto.layer.cornerRadius = 10
        cell.setImage(url: imageURL!)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = URL(string: repoManager.pullRequestList[indexPath.row].url)
        UIApplication.shared.openURL(url!)
    }
    
    // MARK: - Access to Manager
    
    func getPullRequests() {
        self.activityIndicador.startAnimating()
        self.repoManager.getPullRequests(name: self.name, repository: self.repository, callback: { (pullRequests) in
            self.activityIndicador.stopAnimating()
            self.tableView.reloadData()

        })
    }
    
    

}
