//
//  DetailTableViewCell.swift
//  JavaPop
//
//  Created by Mariana Alvarez on 27/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import UIKit
import Alamofire

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    var repoManager = RepoManager.sharedInstance
    var url: String!
    var request: Request?
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setImage(url: String) {
        self.userPhoto.image = nil
        self.request?.cancel()
        
        // Verifying if image is cached
        if let image = self.repoManager.imageCache.image(withIdentifier: "") {
            self.userPhoto.image = image
            return
        }
        
        // Caching image
        request = repoManager.getImage(for: url) { image in
            self.userPhoto.image = image
        }
    }

    
}
