//
//  PullRequest.swift
//  JavaPop
//
//  Created by Mariana Alvarez on 28/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import Foundation

public class PullRequest {
    
    var title: String!
    var body: String!
    var date: String!
    var username: String!
    var userPhoto: String!
    var url: String!
    
}
