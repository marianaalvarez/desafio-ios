//
//  RepoManager.swift
//  JavaPop
//
//  Created by Mariana Alvarez on 28/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireImage

public class RepoManager {
    
    static let sharedInstance = RepoManager()
    let imageCache = AutoPurgingImageCache ()
    
    var repositoryList = [Repository]()
    var pullRequestList = [PullRequest]()
    var currentPage = 1
    
    //MARK: - Connecting to GitHub API

    func getRepositories(callback: @escaping ([Repository]) -> Void) {
        
        Alamofire.request("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(currentPage)").responseJSON { response in
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                let dictionary = json.dictionaryValue
                
                if let array = dictionary["items"]!.array {
                    for item in array {
                        let repo = Repository()
                        repo.repoName = item["name"].stringValue
                        repo.repoDescription = item["description"].stringValue
                        repo.username = item["owner"]["login"].stringValue
                        repo.userPhoto = item["owner"]["avatar_url"].stringValue
                        repo.numberOfForks = item["forks_count"].intValue
                        repo.numberOfStars = item["stargazers_count"].intValue
                    
                        self.repositoryList.append(repo)
                    }
                    callback(self.repositoryList)
                }
                
            } else {
                let error = response.result.error.debugDescription
                print("Error: \(response.result.error.debugDescription)")
                callback(self.repositoryList)
            }
            
            
        }
    }
    
    func getPullRequests(name: String, repository: String, callback: @escaping ([PullRequest]) -> Void) {
        
        let url = "https://api.github.com/repos/\(name)/\(repository)/pulls"
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                
                if let array = json.array {
                    for item in array {
                        let pull = PullRequest()
                        pull.title = item["title"].stringValue
                        pull.body = item["body"].stringValue
                        pull.username = item["user"]["login"].stringValue
                        pull.userPhoto = item["user"]["avatar_url"].stringValue
                        pull.url = item["html_url"].stringValue
                    
                        self.pullRequestList.append(pull)
                    }
                    callback(self.pullRequestList)
                }
            } else {
                let error = response.result.error
                callback(self.pullRequestList)
            }
        }
    }
    
    
    //MARK: - Download Image
    
    func getImage(for url: String, completion: @escaping (UIImage) -> Void) -> Request {
        return Alamofire.request(url, method: .get).responseImage { response in
            guard let image = response.result.value else {
                return
            }
            completion(image)
            self.imageCache.add(image, withIdentifier: url)
        }
    }
    
}
