//
//  JavaPopTests.swift
//  JavaPopTests
//
//  Created by Mariana Alvarez on 27/11/16.
//  Copyright © 2016 Mariana Alvarez. All rights reserved.
//

import XCTest
@testable import JavaPop

class JavaPopTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetRepositories() {
        
        let manager = RepoManager()
        let expectation = self.expectation(description: "Returns list of repositories")
        
        manager.getRepositories( callback: { result in
            
            XCTAssertNotNil(result)
            XCTAssertGreaterThanOrEqual(result.count, 0, "Array is not empty")
            expectation.fulfill()
            
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Error: \(error)")
            }
        }
    }
    
    func testGetPullRequests() {
        
        let manager = RepoManager()
        let expectation = self.expectation(description: "Returns list of pull requests")
        
        let name = "elastic"
        let repository = "elasticsearch"
        
        manager.getPullRequests(name: name, repository: repository) { result in
            
            XCTAssertNotNil(result)
            XCTAssertGreaterThanOrEqual(result.count, 0, "Array is not empty")
            expectation.fulfill()
            
        }
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Error: \(error)")
            }
        }
    }
    
    
}
